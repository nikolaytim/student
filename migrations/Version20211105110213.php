<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211105110213 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE course (id BIGSERIAL NOT NULL, parent_id BIGINT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX course__parent_id__ind ON course (parent_id)');
        $this->addSql('CREATE TABLE lesson (id BIGSERIAL NOT NULL, course_id BIGINT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX lesson__course_id__ind ON lesson (course_id)');
        $this->addSql('CREATE TABLE rating (id BIGSERIAL NOT NULL, task_id BIGINT DEFAULT NULL, student_id BIGINT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, rate INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX rating__task_id__ind ON rating (task_id)');
        $this->addSql('CREATE INDEX rating__student_id__ind ON rating (student_id)');
        $this->addSql('CREATE INDEX rating__created_at__ind ON rating (created_at)');
        $this->addSql('CREATE TABLE skill (id BIGSERIAL NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE spectrum (id BIGSERIAL NOT NULL, skill_id BIGINT DEFAULT NULL, task_id BIGINT DEFAULT NULL, percent INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX spectrum__skill_id__ind ON spectrum (skill_id)');
        $this->addSql('CREATE INDEX spectrum__task_id__ind ON spectrum (task_id)');
        $this->addSql('CREATE TABLE student (id BIGSERIAL NOT NULL, full_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE task (id BIGSERIAL NOT NULL, lesson_id BIGINT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX task__lesson_id__ind ON task (lesson_id)');
        $this->addSql('CREATE TABLE "user" (id BIGSERIAL NOT NULL, login VARCHAR(32) NOT NULL, roles VARCHAR(1024) NOT NULL, password VARCHAR(120) NOT NULL, token VARCHAR(32) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX user__login__ind ON "user" (login)');
        $this->addSql('CREATE UNIQUE INDEX user__token__ind ON "user" (token)');
        $this->addSql('ALTER TABLE course ADD CONSTRAINT course__parent_id__fk FOREIGN KEY (parent_id) REFERENCES course (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT lesson__course_id__fk FOREIGN KEY (course_id) REFERENCES course (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rating ADD CONSTRAINT rating__task_id__fk FOREIGN KEY (task_id) REFERENCES task (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rating ADD CONSTRAINT rating__student_id__fk FOREIGN KEY (student_id) REFERENCES student (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE spectrum ADD CONSTRAINT spectrum__skill_id__fk FOREIGN KEY (skill_id) REFERENCES skill (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE spectrum ADD CONSTRAINT spectrum__task_id__fk FOREIGN KEY (task_id) REFERENCES task (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT task__lesson_id__fk FOREIGN KEY (lesson_id) REFERENCES lesson (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE course DROP CONSTRAINT course__parent_id__fk');
        $this->addSql('ALTER TABLE lesson DROP CONSTRAINT lesson__course_id__fk');
        $this->addSql('ALTER TABLE task DROP CONSTRAINT task__lesson_id__fk');
        $this->addSql('ALTER TABLE spectrum DROP CONSTRAINT spectrum__skill_id__fk');
        $this->addSql('ALTER TABLE rating DROP CONSTRAINT rating__student_id__fk');
        $this->addSql('ALTER TABLE rating DROP CONSTRAINT rating__task_id__fk');
        $this->addSql('ALTER TABLE spectrum DROP CONSTRAINT spectrum__task_id__fk');
        $this->addSql('DROP TABLE course');
        $this->addSql('DROP TABLE lesson');
        $this->addSql('DROP TABLE rating');
        $this->addSql('DROP TABLE skill');
        $this->addSql('DROP TABLE spectrum');
        $this->addSql('DROP TABLE student');
        $this->addSql('DROP TABLE task');
        $this->addSql('DROP TABLE "user"');
    }
}
